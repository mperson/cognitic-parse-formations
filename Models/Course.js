//Utilisation du principe de class (ECMAScript 2015)
class Course {
    //Constructeur de la classe
    constructor(name, categorie, description, lien, duree) {
       this.Name=name;
       this.Categorie=categorie;
       this.Description=description;
       this.Lien= lien;
       this.Duree= duree;
    }

    //Propriété permettant de récupérer les Infos du cours
    get  Infos(){
        return this.Format();
    }

    //Fonction permettant de formatter l'affichage des infos sous format => Nom :  description <br> lien :.... <br> Durée :....
    Format()
    {
        let courte ='';
        //Me permet de ne prendre que 50 caractères de la description
        if(this.Description.length>50)
            courte = this.Description.substring(0,50)+"... ";
        else
            courte = this.Description;

        return `<u>`+ this.Name + `:</u>`+ courte + `<br> <u>Lien : </u><a href='` + this.Lien+`' target='_blank'>`+this.Lien+`</a><br>Durée : `+ this.Duree;
    }

   

  }

  module.exports = Course;