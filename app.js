/*Librairie externe*/
const { exec } = require("child_process"); //Librairie permettant de lancer un processus en ligne de commande
/*Modules interne*/ 
const CustomParser = require('./Modules/CustomParser');
/*Variables locales*/
const url = 'https://cognitic.be/nos-formations?field_course_category_target_id=All&field_course_level_value=All&title=&page=1';
const path = './Output/Formations.html';


CustomParser.getPage(url).then(response => {
 
    if (response.data) {  

      let parse = CustomParser.parsePage(response.data);
      CustomParser.savePage(parse,path);

      let url = '\''+__dirname.replace("\\","/")+"/Output/Formations.html'";
      
      exec('"'+__dirname.replace("\\","/")+'/Output/Formations.html"', (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            return;
        }
        console.log(`stdout: ${stdout}`);
    });
    }
  })
  .catch(error => {
    console.log(error)
  });

